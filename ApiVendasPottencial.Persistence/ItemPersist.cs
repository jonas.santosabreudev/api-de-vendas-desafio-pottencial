using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence.Contexto;
using ApiVendasPottencial.Persistence.Contratos;
using Microsoft.EntityFrameworkCore;

namespace ApiVendasPottencial.Persistence
{
    public class ItemPersist : IItemPersist
    {
        private readonly ApiContext _apiContext;
        public ItemPersist(ApiContext apiContext)
        {
            _apiContext = apiContext;
        }

         public async Task<Item> BuscarItemPorId(int id)
        {
            IQueryable<Item> query = _apiContext.Itens.AsNoTracking().Where(x => x.Id == id);
            return await query.FirstOrDefaultAsync();
        }
        public async Task<List<Item>> BuscarTodosItens()
        {
            var result = _apiContext.Itens.AsNoTracking().ToListAsync();
            return await result;
        }
    }
}