using ApiVendasPottencial.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiVendasPottencial.Persistence.Contexto
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options){}
        public DbSet<Item> ?Itens {get; set; }
        public DbSet<Venda> ?Vendas {get; set; }
        public DbSet<Vendedor> ?Vendedores {get; set; }

        //  protected override void OnModelCreating(ModelBuilder modelBuilder)
        // {
        //     base.OnModelCreating(modelBuilder);
        // }
    }
}