using ApiVendasPottencial.Domain.Models;

namespace ApiVendasPottencial.Persistence.Contratos
{
    public interface IVendaPersist
    {
         Task<Venda> BuscarVendaPorId(int id);
    }
}