using ApiVendasPottencial.Domain.Models;

namespace ApiVendasPottencial.Persistence.Contratos
{
    public interface IItemPersist
    {
         Task<Item> BuscarItemPorId(int id);
         Task<List<Item>> BuscarTodosItens();
    }
}