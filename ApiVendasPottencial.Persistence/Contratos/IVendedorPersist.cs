using ApiVendasPottencial.Domain.Models;

namespace ApiVendasPottencial.Persistence.Contratos
{
    public interface IVendedorPersist
    {
         Task<Vendedor> BuscarVendedorPorId(int id);
         Task<List<Vendedor>> BuscarTodosVendedores();
    }
}