using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence.Contexto;
using ApiVendasPottencial.Persistence.Contratos;
using Microsoft.EntityFrameworkCore;

namespace ApiVendasPottencial.Persistence
{
    public class VendedorPersist : IVendedorPersist
    {
        private readonly ApiContext _apiContext;
        public VendedorPersist(ApiContext apiContext)
        {
            _apiContext = apiContext;
        }

         public async Task<Vendedor> BuscarVendedorPorId(int id)
        {
            IQueryable<Vendedor> query = _apiContext.Vendedores.AsNoTracking().Where(x => x.Id == id);
            return await query.FirstOrDefaultAsync();
        }
        public async Task<List<Vendedor>> BuscarTodosVendedores()
        {
            var result = _apiContext.Vendedores.AsNoTracking().ToListAsync();
            return await result;
        }
    }
}