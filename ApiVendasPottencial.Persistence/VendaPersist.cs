using System.Data.SqlClient;
using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence.Contexto;
using ApiVendasPottencial.Persistence.Contratos;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ApiVendasPottencial.Persistence
{
    public class VendaPersist : IVendaPersist
    {
        private readonly ApiContext _apiContext;

        public VendaPersist(ApiContext apiContext)
        {
            _apiContext = apiContext;
        }

         public async Task<Venda> BuscarVendaPorId(int id)
        {
            IQueryable<Venda> query = _apiContext.Vendas
            .Include(x => x.Vendedor)
            .Include(x => x.Item);

            query = query.AsNoTracking().Where(x => x.Id == id);
            return await query.FirstOrDefaultAsync();
        }
    }
}