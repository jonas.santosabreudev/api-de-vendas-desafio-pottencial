using System.ComponentModel.DataAnnotations;

namespace ApiVendasPottencial.Domain.Models
{
    public class Item
    {
        [Key()]
        public int Id { get; set; }
        public string Descricao { get; set; }
        public double Valor { get; set; }
    }
}