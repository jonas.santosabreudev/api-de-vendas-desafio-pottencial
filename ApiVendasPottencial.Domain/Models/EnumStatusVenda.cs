namespace ApiVendasPottencial.Domain.Models
{
    public enum EnumStatusVenda
    {
        Aguadando_Pagamento = 1,

        Pagamento_Aprovado = 2,

        Enviado_para_transportadora  =3,

        Entregue = 4,

        Cancelado =5
    }
}