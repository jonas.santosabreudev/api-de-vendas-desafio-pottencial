using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence.Contratos;
using ApiVendasPottencial.Service.Dtos;
using AutoMapper;

namespace ApiVendasPottencial.Service
{
    public class ItemService : IItemService
    {
        private readonly IGeralPersist _geralPersist;
        private readonly IItemPersist _item;
        private readonly IMapper _mapper;
        public ItemService(IGeralPersist geralPersist, IMapper mapper, IItemPersist item)
        {
            _geralPersist = geralPersist;
            _mapper = mapper;
            _item = item;
        }
        public async Task<ItemDto> AdicionarItem(ItemDto model)
        {
            try
            {
               var item = _mapper.Map<Item>(model);
               _geralPersist.Add<Item>(item);
                if(await _geralPersist.SaveChangesAsync())
                {
                    var itemRetorno = await BuscarItemPorId(item.Id);
                    return _mapper.Map<ItemDto>(itemRetorno);
                }
                return null;
            }
            catch (Exception ex)
            {
            throw new Exception(ex.Message);
            }
        }
        public async Task<ItemDto> AtualizarItem(int id, ItemDto model)
        {
            try
            {
                var itemResult = await _item.BuscarItemPorId(id);
                if(itemResult == null) return null;

                _mapper.Map(model, itemResult);
                _geralPersist.Update<Item>(itemResult);

                if(await _geralPersist.SaveChangesAsync())
                {
                    var itemRetorno = await _item.BuscarItemPorId(itemResult.Id);
                    return _mapper.Map<ItemDto>(itemRetorno);
                }
            return null;
            }
            catch (Exception ex)
            {
                 throw new Exception(ex.Message);
            }
        }
        public async Task<bool> DeletarItem(int id)
        {
            try
            {
                var itemResult = await _item.BuscarItemPorId(id);
                if(itemResult == null) throw new Exception("Item para delete não encontrado.");

                _geralPersist.Delete<Item>(itemResult);
                return await _geralPersist.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ItemDto> BuscarItemPorId(int id)
        {
            var item = await _item.BuscarItemPorId(id);
            if(item == null) return null;

            var resultado = _mapper.Map<ItemDto>(item);

            return resultado;
        }
        public async Task<List<Item>> BuscarTodos()
        {
            var item = await _item.BuscarTodosItens();
            if(item == null) return null;

            return item.ToList();
        }
    }
}