using System.ComponentModel.DataAnnotations;

namespace ApiVendasPottencial.Service.Dtos
{
    public class ItemDto
    {
        [Key()]
        public int Id { get; set; }
        public string Descricao { get; set; }
        public double Valor { get; set; }
    }
}