using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ApiVendasPottencial.Domain.Models;

namespace ApiVendasPottencial.Service.Dtos
{
    public class VendaDto 
    {
        [Key()]
        public int Id { get; set; }
        [ForeignKey("Vendedor")]
        public int VendedorId { get; set; }
        public virtual Vendedor? Vendedor { get; set; }
        [ForeignKey("Item")]
        public int ItemId { get; set; }
        public virtual Item? Item { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda? Status { get; set; }
    }
}

