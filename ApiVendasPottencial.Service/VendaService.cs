using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence;
using ApiVendasPottencial.Persistence.Contratos;
using ApiVendasPottencial.Service.Dtos;
using AutoMapper;

namespace ApiVendasPottencial.Service
{
    public class VendaService : IVendaService
    {
        private readonly IGeralPersist _geralPersist;
        private readonly IVendaPersist _vendaPersist;
        private readonly IMapper _mapper;
        public VendaService(IGeralPersist geralPersist, IMapper mapper, IVendaPersist vendaPersist)
        {
            _geralPersist = geralPersist;
            _mapper = mapper;
            _vendaPersist = vendaPersist;
        }
        public async Task<VendaDto> AdicionarVenda(VendaDto model)
        {
            try
            {
               var venda = _mapper.Map<Venda>(model);
               _geralPersist.Add<Venda>(venda);
                if(await _geralPersist.SaveChangesAsync())
                {
                    var vendaRetorno = await BuscarVendaPorId(venda.Id);
                    return _mapper.Map<VendaDto>(vendaRetorno);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<VendaDto> AtualizarVendedor(int id, VendaDto model)
        {
            try
            {
                var pedidoBd = await _vendaPersist.BuscarVendaPorId(id);
                if (pedidoBd == null) return null;

                var valida = ValidaStatus(model, pedidoBd);

                if(valida == "Atualização não disponível"){
                    return null;
                }
                _mapper.Map(model, pedidoBd);
                _geralPersist.Update<Venda>(pedidoBd);

                if (await _geralPersist.SaveChangesAsync())
                {
                    var vendaRetorno = await _vendaPersist.BuscarVendaPorId(pedidoBd.Id);
                    return _mapper.Map<VendaDto>(pedidoBd);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string ValidaStatus(VendaDto model, Venda pedidoBd)
        {
            switch (pedidoBd.Status.Value)
            {
                case EnumStatusVenda.Aguadando_Pagamento:
                    if (model.Status == EnumStatusVenda.Pagamento_Aprovado)
                    {
                        pedidoBd.Status = EnumStatusVenda.Pagamento_Aprovado;
                    }
                    else if (model.Status == EnumStatusVenda.Cancelado)
                    {
                        pedidoBd.Status = EnumStatusVenda.Cancelado;
                    }
                    else
                    {
                        return "Atualização não disponível";
                    }
                    break;
                case EnumStatusVenda.Pagamento_Aprovado:
                    if (model.Status == EnumStatusVenda.Enviado_para_transportadora)
                    {
                        pedidoBd.Status = EnumStatusVenda.Enviado_para_transportadora;
                    }
                    else if (model.Status == EnumStatusVenda.Cancelado)
                    {
                        pedidoBd.Status = EnumStatusVenda.Cancelado;
                    }
                    else
                    {
                        return "Atualização não disponível";
                    }
                    break;
                case EnumStatusVenda.Enviado_para_transportadora:
                    if (model.Status == EnumStatusVenda.Entregue)
                    {
                        pedidoBd.Status = EnumStatusVenda.Entregue;
                    }
                    else
                    {
                        return "Atualização não disponível";
                    }
                    break;
                default:
                    return "Atualização não disponível";
            }
            return "Atualização não disponível";
        }

        public async Task<VendaDto> BuscarVendaPorId(int id)
        {
            try
            {
               var venda = await _vendaPersist.BuscarVendaPorId(id);
               if(venda == null) return null;

               var resultado = _mapper.Map<VendaDto>(venda);
               return resultado;
            }
            catch (Exception ex)
            {
                 throw new Exception(ex.Message);
            }
        }
    }
}