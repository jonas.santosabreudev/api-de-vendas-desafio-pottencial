using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Service.Dtos;
using AutoMapper;

namespace ApiVendasPottencial.Service.Helpers
{
    public class ApiProfile : Profile
    {
        public ApiProfile()
        {
            CreateMap<Item, ItemDto>().ReverseMap();
        }
    }
}