using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence.Contratos;
using ApiVendasPottencial.Service.Dtos;
using AutoMapper;

namespace ApiVendasPottencial.Service
{
    public class VendedorService : IVendedorService
    {
        private readonly IGeralPersist _geralPersist;
        private readonly IVendedorPersist _vendedor;
        private readonly IMapper _mapper;
        public VendedorService(IGeralPersist geralPersist, IMapper mapper, IVendedorPersist vendedor)
        {
            _geralPersist = geralPersist;
            _mapper = mapper;
            _vendedor = vendedor;
        }
        public async Task<VendedorDto> AdicionarVendedor(VendedorDto model)
        {
            try
            {
               var vendedor = _mapper.Map<Vendedor>(model);
               _geralPersist.Add<Vendedor>(vendedor);
                if(await _geralPersist.SaveChangesAsync())
                {
                    var vendedorRetorno = await BuscarVendedorPorId(vendedor.Id);
                    return _mapper.Map<VendedorDto>(vendedorRetorno);
                }
                return null;
            }
            catch (Exception ex)
            {
            throw new Exception(ex.Message);
            }
        }
        public async Task<VendedorDto> AtualizarVendedor(int id, VendedorDto model)
        {
            try
            {
                var vendedorResult = await _vendedor.BuscarVendedorPorId(id);
                if(vendedorResult == null) return null;

                _mapper.Map(model, vendedorResult);
                _geralPersist.Update<Vendedor>(vendedorResult);

                if(await _geralPersist.SaveChangesAsync())
                {
                    var vendedorRetorno = await _vendedor.BuscarVendedorPorId(vendedorResult.Id);
                    return _mapper.Map<VendedorDto>(vendedorRetorno);
                }
            return null;
            }
            catch (Exception ex)
            {
                 throw new Exception(ex.Message);
            }
        }
        public async Task<bool> DeletarVendedeor(int id)
        {
            try
            {
                var vendedorResult = await _vendedor.BuscarVendedorPorId(id);
                if(vendedorResult == null) throw new Exception("vendedor para delete não encontrado.");

                _geralPersist.Delete<Vendedor>(vendedorResult);
                return await _geralPersist.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<VendedorDto> BuscarVendedorPorId(int id)
        {
            var vendedor = await _vendedor.BuscarVendedorPorId(id);
            if(vendedor == null) return null;

            var resultado = _mapper.Map<VendedorDto>(vendedor);

            return resultado;
        }
        public async Task<List<Vendedor>> BuscarTodos()
        {
            var vendedor = await _vendedor.BuscarTodosVendedores();
            if(vendedor == null) return null;

            return vendedor.ToList();
        }
    }
}