using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence;
using ApiVendasPottencial.Persistence.Contratos;
using ApiVendasPottencial.Service.Dtos;
using AutoMapper;

namespace ApiVendasPottencial.Service
{
    public interface IVendaService
    {
      Task<VendaDto> AdicionarVenda(VendaDto model);
      Task<VendaDto> AtualizarVendedor(int id, VendaDto model);
      Task<VendaDto> BuscarVendaPorId(int id);
    }
}