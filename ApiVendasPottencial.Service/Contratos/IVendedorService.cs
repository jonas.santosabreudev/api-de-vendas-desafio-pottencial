using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence.Contratos;
using ApiVendasPottencial.Service.Dtos;
using AutoMapper;

namespace ApiVendasPottencial.Service
{
    public interface IVendedorService
    {
      Task<VendedorDto> AdicionarVendedor(VendedorDto model);
      Task<VendedorDto> AtualizarVendedor(int id, VendedorDto model);
      Task<VendedorDto> BuscarVendedorPorId(int id);
      Task<List<Vendedor>> BuscarTodos();
      Task<bool> DeletarVendedeor(int id); 
    }
}