using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence.Contratos;
using ApiVendasPottencial.Service.Dtos;
using AutoMapper;

namespace ApiVendasPottencial.Service
{
    public interface IItemService
    {
      Task<ItemDto> AdicionarItem(ItemDto model);
      Task<ItemDto> AtualizarItem(int id, ItemDto model);
      Task<ItemDto> BuscarItemPorId(int id);
      Task<List<Item>> BuscarTodos();
      Task<bool> DeletarItem(int id); 
    }
}