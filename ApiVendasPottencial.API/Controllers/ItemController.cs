using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence.Contexto;
using ApiVendasPottencial.Persistence.Contratos;
using ApiVendasPottencial.Service;
using ApiVendasPottencial.Service.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace ApiVendasPottencial.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    
    public class ItemController : ControllerBase
    {
        private readonly IItemService _itemService;

        public ItemController(IItemService itemService)
        {
            _itemService = itemService;
        }
        [HttpPost("CriarItem")]
        public async Task<IActionResult> CriarItem(ItemDto item)
        {
            try
            {
                var result = await _itemService.AdicionarItem(item);
                if(result == null) return NoContent();
                
                return CreatedAtAction(nameof(ObterItemPorId), new { id = item.Id }, result);  

            }
             catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar adicionar item. Erro: {ex.Message}");
            }
        }
        [HttpGet("BuscarItemPorId")]
        public async Task<IActionResult> ObterItemPorId(int id)
        {
            try
            {
                var item = await _itemService.BuscarItemPorId(id);
                if (item == null) return NoContent();

                return Ok(item);
            }
            catch (Exception ex)
            {
                 return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar recuperar Item. Erro: {ex.Message}");
            }
        }
        [HttpGet("BuscarTodos")]
        public async Task<IActionResult> ObterItens()
        {
            try
            {
                var item = await _itemService.BuscarTodos();
                if(item == null) return NoContent();

                return Ok(item);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar recuperar todos os itens. Erro: {ex.Message}");
            }
        }
        [HttpDelete("DeletarItem")]
        public async Task<IActionResult> Deletar(int id)
        {
            try
            {
                var item = await _itemService.BuscarItemPorId(id);
                if(item == null) return NoContent();

                return await _itemService.DeletarItem(id)
                    ? Ok(new { message = "Item deletado"})
                    : throw new Exception("Ocorreu um problem não específico ao tentar deletar Item.");
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Erro ao tentar deletar item. Erro: {ex.Message}");
            }
        }
        [HttpPut("Atualizar")]
        public async Task<IActionResult> Atualizar(int id, ItemDto model)
        {
            try
            {
                var item = await _itemService.AtualizarItem(id, model);
                if(item == null) return NoContent();

                return Ok(item);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar atualizar o item. Erro: {ex.Message}");
            }
        }
    }
}