using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence.Contexto;
using ApiVendasPottencial.Service;
using ApiVendasPottencial.Service.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace ApiVendasPottencial.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly IVendedorService _vendedorService;
        private readonly IItemService _itemService;
        private readonly IVendaService _vendaService;

        public VendaController(IVendedorService vendedorService, IItemService itemService, IVendaService vendaService)
        {
            _vendedorService = vendedorService;
            _itemService = itemService;
            _vendaService = vendaService;
        }
        [HttpPost("CadastrarVenda")]
        public async Task<IActionResult> InserirVenda(VendaDto model)
        {
            try
            {
               model.Status = EnumStatusVenda.Aguadando_Pagamento;
               model.Data = DateTime.Now;

                var vendedor = _vendedorService.BuscarVendedorPorId(model.VendedorId);
                if(vendedor == null){
                    return BadRequest(new { Erro = "Vendedor Não Cadastrado" });
                }
                var item = _itemService.BuscarItemPorId(model.Id);
                if(item == null){
                    return BadRequest(new { Erro = "Item não cadastrado!" });
                }
                
                await _vendaService.AdicionarVenda(model);
                
                return CreatedAtAction(nameof(ObterVendaPorId), new { id = model.Id }, model); 
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar adicionar venda. Erro: {ex.Message}");
            }  
        }
        [HttpGet("BuscarVendaPorId")]
        public async Task<IActionResult> ObterVendaPorId(int id)
        {
            try
            {
                var venda = await _vendaService.BuscarVendaPorId(id);
                if(venda == null) return NoContent();
                return Ok(venda);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar obter venda. Erro: {ex.Message}");
            }
        }
        
        [HttpPut("Atualizar")]
        public async Task<IActionResult> Atualizar(int id, VendaDto pedido)
        {
            try
            {
                var result = await _vendaService.AtualizarVendedor(id, pedido);
                if(result == null) return BadRequest(new { Erro = "Atualização não disponivel!" }); 
                return Ok(result);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar atualizar venda. Erro: {ex.Message}");
            }
        }
    }
}