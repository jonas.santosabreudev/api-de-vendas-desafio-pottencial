using ApiVendasPottencial.Domain.Models;
using ApiVendasPottencial.Persistence.Contexto;
using ApiVendasPottencial.Service;
using ApiVendasPottencial.Service.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace ApiVendasPottencial.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    
    public class VendedorController : ControllerBase
    {
        private readonly IVendedorService _vendedorService;
        public VendedorController(IVendedorService vendedorService)
        {
            _vendedorService = vendedorService;
        }
        [HttpPost("CadastrarVendedor")]
        public async Task<IActionResult> CriarVendedor(VendedorDto model)
        {
            try
            {
                var result = await _vendedorService.AdicionarVendedor(model);
                if(result == null) return NoContent();

                return CreatedAtAction(nameof(ObterVendedorPorId), new { id = model.Id }, result);     
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar adicionar Vendedor. Erro: {ex.Message}");
            }
        }
        [HttpGet("BuscarVendedorPorId")]
        public async Task<IActionResult> ObterVendedorPorId(int id)
        {
            try
            {
                var vendedor = await _vendedorService.BuscarVendedorPorId(id);
                if(vendedor == null)return NoContent();

                return Ok(vendedor);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar obter vendedor. Erro: {ex.Message}");
            }
        }
        [HttpGet("BuscarTodosVendedores")]
        public async Task<IActionResult> ObterVendedores()
        {
            try
            {
                var vendedor = await _vendedorService.BuscarTodos();
                if(vendedor == null) return NoContent();

                return Ok(vendedor);
            }
            catch (Exception ex)
            {
                 return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar recuperar todos os vendedores. Erro: {ex.Message}");
            }
        }
        [HttpDelete("DeletarVendedor")]
        public async Task<IActionResult> Deletar(int id)
        {
            try
            {
                var vendedor = await _vendedorService.BuscarVendedorPorId(id);
                if(vendedor == null) return NoContent();

                return await _vendedorService.DeletarVendedeor(id)
                    ? Ok(new { message = "vendedor deletado"})
                    : throw new Exception("Ocorreu um problem não específico ao tentar deletar Item.");
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Erro ao tentar deletar vendedor. Erro: {ex.Message}");
            }
        }
        [HttpPut("AtualizarVendedor")]
        public async Task<IActionResult> Atualizar(int id, VendedorDto model)
        {
            try
            {
                var vendedor = await _vendedorService.AtualizarVendedor(id, model);
                if(vendedor == null) return NoContent();

                return Ok(vendedor);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Erro ao tentar atualizar vendedor. Erro: {ex.Message}");
            }
        }
    }
}