using ApiVendasPottencial.Persistence;
using ApiVendasPottencial.Persistence.Contexto;
using ApiVendasPottencial.Persistence.Contratos;
using ApiVendasPottencial.Service;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<ApiContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("ConexaoPadrao")));
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddTransient<ApiContext>();

builder.Services.AddScoped<IGeralPersist, GeralPersist>();
builder.Services.AddScoped<IItemPersist, ItemPersist>();
builder.Services.AddScoped<IItemService, ItemService>();
builder.Services.AddScoped<IVendedorService, VendedorService>();
builder.Services.AddScoped<IVendedorPersist, VendedorPersist>();
builder.Services.AddScoped<IVendaPersist, VendaPersist>();
builder.Services.AddScoped<IVendaService, VendaService>();


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
